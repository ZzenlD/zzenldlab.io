import glob
import os
import re
import sys

# from deep_translator import GoogleTranslator
# from deep_translator import DeepL

translate_to = ["en"]
system_keywords = ["title", "date", "tags"]
header_separator = "+++"

if len(sys.argv) > 1:
    input = str(sys.argv[1])
else:
    input = ""


def translate(path):
    print(os.getenv("HOME"))
    dirname = os.path.dirname(path)
    filename = os.path.basename(path)
    unique_filename = re.findall(r"([\w|-]+)\.de\.md", filename)[0]
    for lang in translate_to:
        string = dirname + "/" + unique_filename + "." + lang + ".md"

        # Add fist line
        lang_file = open(string, "w")
        lang_file.write("1" + header_separator)
        lang_file.close()

        # Read each de.md-file and translate it
        de_file = open(path, "r")
        lang_file = open(string, "w")
        de_file.seek(1)
        end_of_header = False
        for line in de_file.readlines():

            if end_of_header is False:
                if line == header_separator + "\n":
                    end_of_header = True
                    lang_file.write("1" + header_separator + "\n")
                else:
                    print(re.split(r"\=", line))
                    lang_file.write("1" + line)
            else:
                lang_file.write(line)

        de_file.close()
        lang_file.close()
        exit
        #        f.write(GoogleTranslator(source="de",
        #                                 target = lang).translate_file(path))
        #        f.write(DeepL(api_key=os.getenv("DEEPL_API_KEY"), source="de",
        #                      target=lang, use_free_api=True).translate(path))
        #        f.close()
        print("Translated to:", string)


# Update all files
if input == "all":
    for file in glob.glob("content/**/*.de.md"):
        translate(file)

# Update only given files
elif input:
    translate(input)

# Update all changed or added files
else:
    print("Alle geänderten/hinzugefügten Datein aus git status generieren")
