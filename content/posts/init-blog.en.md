---
title: "$ init blog"
date: 2021-05-25
tags:
  - Welcome
---

Maybe in the next 100 years I will find the time to actually write an article. But right now, that's all I'm going to do. Future posts may include various IT and decentralization topics.
