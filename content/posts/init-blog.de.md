---
title: "$ init blog"
date: 2021-05-25
tags:
  - Willkommen
---

Vielleicht finde ich in den nächsten 100 Jahren mal die Zeit, tatsächlich einen Beitrag zu schreiben. Aber momentan ist dies alles, was ich tun werde. Zukünftige Posts können verschiedene Themen aus dem Bereich IT und Dezentralisierung beinhalten.
