+++
title = "Projects"
+++

## Open source projects

- [EnvC] (<https://gitlab.com/zzenld/envc>): Framework for the fast and secure construction of environments with little effort
- [zbfl] (<https://gitlab.com/zzenld/zbfl>): Bash library for standardizing input and output functions
- [gitlab-ci] (<https://gitlab.com/zzenld/gitlab-ci>): Collection of predefined Gitlab pipelines for easy use

## Open source contributions

- [awesome-github-profile-readme] (<https://github.com/abhisheknaiidu/awesome-github-profile-readme/pull/568>): Added my profile in README to "Minimalistic"

## Supervised environments

#### Pfaffenhofen fire brigade

The Pfaffenhofen fire brigade operates an extensive hybrid IT environment, both on-site in the tool shed and cloud-based. High stability and security with the lowest possible fees are the credo here because it is a voluntary service with limited resources.
In order to meet the tasks and obligations placed on the defense, the landscape is constantly updated, optimized and further developed.

**Customer:** Pfaffenhofen volunteer fire brigade

**Technologies:** [Terraform] (<https://www.terraform.io/>), [Ansible] (<https://www.ansible.com/>), [Docker] (https: //www.docker. com /), [Nextcloud] (<https://nextcloud.com/>), [Mail-in-a-Box] (<https://mailinabox.email/>), [QNAP] (https: //www.qnap. com /), [UniFi] (<https://www.ui.com/>), [Chocolatey] (<https://chocolatey.org/>)

**Website:** [https://ff-pfaffenhofen.de] (<https://ff-pfaffenhofen.de>)

---

#### private cloud

The aim of this cloud is to show that a high level of comfort is also possible with data protection, privacy and hosting in Germany, without Google, Amazon, Microsoft. The environment is based on pure open source solutions, is designed for maximum security and is available to me, my family and various friends and acquaintances who are interested in it.

**Customer:** Me, family, friends, acquaintances

**Technologies:** [Rancher] (<https://rancher.com/products/rancher/>), [Nextcloud] (<https://nextcloud.com/>), [Mailcow] (<https://mailcow.email/> ), [SearX] (<https://searx.me/>), [WireGuard] (<https://www.wireguard.com/>), [Traefik] (<https://traefik.io/>)

**Website:** [https://nextcloud.ulimate.cloud] (<https://nextcloud.ultimate.cloud>)

---

#### HomeLab

I run this small HomeLab at home for various tests, new experiences and learning.

**Customer:** i

**Technologies:** [Proxmox] (<https://www.proxmox.com/>), [Rancher] (<https://rancher.com/products/rancher/>), [Ansible] (https: // www. ansible.com/), [Pi-hole] (<https://pi-hole.net/>), [Home Assistant] (<https://www.home-assistant.io/>), [QNAP] (https: / /www.qnap.com/), [UniFi] (<https://www.ui.com/>)

---

#### private households

I was able to make it clear to family, as well as some friends and acquaintances, that data protection and data backup are essential nowadays. That is why I have been looking after small environments for several years, which I check and maintain at regular intervals.

**Customer:** Family, friends, acquaintances

**Technologies:** [Ansible] (<https://www.ansible.com/>), [QNAP] (<https://www.qnap.com/>), [Chocolatey] (<https://chocolatey.org/> )
