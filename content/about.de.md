+++
title = "About"
+++

---

Inhaltsverzeichnis

- [Fähigkeiten](#fähigkeiten)
- [Zertifizierungen](#zertifizierungen)
- [Interessen](#interessen)
- [Anfragen und Kontakt](#anfragen-und-kontakt)

---

Hi, ich bin ZzenlD.

Schön dich in meiner kleinen Ecke des Internets zu sehen.

## Fähigkeiten

**Entwicklung & Cloud:** [GitLab](https://about.gitlab.com/), [Rancher](https://rancher.com/products/rancher/), [Artifactory](https://jfrog.com/artifactory/), [Traefik](https://traefik.io/) \
**Virtualisierung & Automatisierung:** [Terraform](https://www.terraform.io/), [cloud-init](https://cloud-init.io/), [Ansible](https://www.ansible.com/), [VMware](https://www.vmware.com/), [Proxmox](https://www.proxmox.com/) \
**Administration & Tools:** [Nextcloud](https://nextcloud.com/), [OnlyOffice](https://www.onlyoffice.com/), [Keycloak](https://www.keycloak.org/), [i-doit](https://www.i-doit.org/), [JDisc](https://www.jdisc.com/en/), [CheckMK](https://checkmk.com/), [Nessus](https://tenable.com/products/nessus), [Duplicati](https://www.duplicati.com/) \
**Betriebssysteme:** [Debian](https://www.debian.org/), [ArchLinux](https://archlinux.org/), [RKE](https://rancher.com/products/rke/), [K3S](https://k3s.io/) \
**Netzwerk & Sicherheit:** Routing/Switching-Grundlagen, Firewall, DNS \
**Programmierung & Scripting:** [Bash](<https://wikipedia.org/wiki/Bash_(Shell)>), [Python](https://www.python.org/), [Git](https://git-scm.com/)

---

## Zertifizierungen

- [ISTQB-FL](https://www.istqb.org/): Certified Tester
- [LPIC-1](https://www.lpi.org/our-certifications/lpic-1-overview): Linux Professional

---

## Interessen

- Allgemeinwohl
- Dinge einfach halten
- entwickeln
- Umweltbewusst
- Open Source
- schwimmen
- Minimalismus
- finanzielle Freiheit
- papierlos
- kochen
- Reisen
- Ehrenamt
- Teamarbeit
- Feuerwehr
- Automatisierung

---

## Anfragen und Kontakt

Kontaktieren Sie mich gerne via [GitLab](https://gitlab.com/zzenld) oder [GitHub](https://github.com/zzenld).
