+++
title = "Impressum"
+++

## Angaben gemäß § 5 TMG und Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV

M. Zender \
St. Martin Straße 12 \
D-89284 Pfaffenhofen

## Kontakt

E-Mail: zzenld@posteo.de

## Datenschutz

Aus einer langen Tradition aus Interesse und tiefer Liebe zum Datenschutz teile ich Euch in dieser Datenschutzerklärung mit, dass ich nicht von Euch wissen will, wer Ihr seid, wann Ihr Euch auf dieser Website widerfindet. Ich will auch nicht wissen, wie lange Ihr hier verweilt, was Ihr Euch anseht oder sogar welche Website Ihr danach besucht.

Weil mich Euer Verhalten auf meiner Website nichts angeht, erhebe ich auch keine Daten über Euer Nutzerverhalten und schreibe auch keine Serverlogs mit.

Datenschutz beginnt und endet immer mit personenbezogenen Daten. Weil die Fragen, wann ein Datum personenbezogen ist, so wichtig ist, hat der europäische Gesetzgeber eine Legaldefinition hierfür im Art. 4 Lit.1 DSGVO eingefügt:

Bei „personenbezogene Daten“ handelt es sich um alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person ( „betroffene Person“) beziehen; als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind.
