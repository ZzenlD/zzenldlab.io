+++
title = "Projekte"
+++

## Open Source Projekte

- [EnvC](https://gitlab.com/zzenld/envc): Framework zum schnellen und sicheren Aufbau von Umgebungen mit wenig Aufwand
- [zbfl](https://gitlab.com/zzenld/zbfl): Bash-Bibliothek zur Vereinheitlichung von Ein- und Ausgabefunktionen
- [gitlab-ci](https://gitlab.com/zzenld/gitlab-ci): Sammlung von vordefinierten Gitlab-Pipelines zur einfachen Verwendung

## Open Source Beiträge

- [awesome-github-profile-readme](https://github.com/abhisheknaiidu/awesome-github-profile-readme/pull/568): Added my profile in README to "Minimalistic"

## Betreute Umgebungen

#### Feuerwehr Pfaffenhofen

Die Feuerwehr Pfaffenhofen betreibt eine umfangreiche hybride IT-Umgebung, sowohl vor Ort im Gerätehaus alsauch cloudbasiert. Hohe Stabilität und Sicherheit bei möglichst niedrigen Gebühren sind hier das Credo da es sich um eine ehrenamtliche Wehr mit begrenzten Mitteln handelt.
Um den an die Wehr gestellten Aufgaben und Pflichten nachzukommen wird die Landschaft stetig aktualisiert, optimiert und weiterentwickelt.

**Kunde:** Freiwillige Feuerwehr Pfaffenhofen

**Technologien:** [Terraform](https://www.terraform.io/), [Ansible](https://www.ansible.com/), [Docker](https://www.docker.com/), [Nextcloud](https://nextcloud.com/), [Mail-in-a-Box](https://mailinabox.email/), [QNAP](https://www.qnap.com/), [UniFi](https://www.ui.com/), [Chocolatey](https://chocolatey.org/)

**Website:** [https://ff-pfaffenhofen.de](https://ff-pfaffenhofen.de)

---

#### Private Cloud

Ziel dieser Cloud ist es zu zeigen das hoher Komfort auch mit Datenschutz, Privatsphäre und Hosting in Deutschland möglich ist, ganz ohne Google, Amazon, Microsoft. Die Umgebung basiert auf reinen Open Source-Lösungen, ist auf maximale Sicherheit ausgelegt und steht mir, meiner Familie, sowie verschiedenen Freunden und Bekannten zur Verfügung die daran Interesse haben.

**Kunde:** Ich, Familie, Freunde, Bekannte

**Technologien:** [Rancher](https://rancher.com/products/rancher/), [Nextcloud](https://nextcloud.com/), [Mailcow](https://mailcow.email/), [SearX](https://searx.me/), [WireGuard](https://www.wireguard.com/), [Traefik](https://traefik.io/)

**Website:** [https://nextcloud.ulimate.cloud](https://nextcloud.ultimate.cloud)

---

#### HomeLab

Für verschiedene Tests, neue Erfahrungen und zum Erlernen betreibe ich dieses kleine HomeLab zuhause.

**Kunde:** Ich

**Technologien:** [Proxmox](https://www.proxmox.com/), [Rancher](https://rancher.com/products/rancher/), [Ansible](https://www.ansible.com/), [Pi-hole](https://pi-hole.net/), [Home Assistant](https://www.home-assistant.io/), [QNAP](https://www.qnap.com/), [UniFi](https://www.ui.com/)

---

#### Privathaushalte

Der Familie, sowie einigen Freunden und Bekannten konnte ich klarmachen das Datenschutz und Datensicherung essenziell heutzutage sind. Daher betreue ich seit mehrere Jahren nun kleine Umgebungen, welche ich in regelmäßigen Abständen überprüfe und warte.

**Kunde:** Familie, Freunde, Bekannte

**Technologien:** [Ansible](https://www.ansible.com/), [QNAP](https://www.qnap.com/), [Chocolatey](https://chocolatey.org/)
