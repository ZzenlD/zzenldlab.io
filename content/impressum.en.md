+++
title = "Imprint"
+++

## Information according to § 5 TMG and responsible for the content according to § 55 Abs. 2 RStV

M. Zender \
St. Martin Street 12 \
D-89284 Pfaffenhofen

## Contact

Email: zzenld@posteo.de

## Privacy

Out of a long tradition of interest and deep love for data protection, I would like to inform you in this data protection declaration that I do not want to know who you are if you find yourself on this website. I also don't want to know how long you stay here, what you look at or even which website you visit afterwards.

Because your behavior on my website does not concern me, I do not collect any data about your user behavior and do not write any server logs.

Data protection always begins and ends with personal data. Because the question of when a date is personal is so important, the European legislator has inserted a legal definition for this in Art. 4 lit.1 GDPR:

"Personal data" is all information that relates to an identified or identifiable natural person ("data subject"); A natural person is regarded as identifiable who can be identified directly or indirectly, in particular by means of assignment to an identifier such as a name, an identification number, location data, an online identifier or one or more special features that express the physical , physiological, genetic, psychological, economic, cultural or social identity of this natural person.
